# PRUDENCE, MÈRE DE SÛRETÉ

Le projet *Prudence, mère de sûreté* a pour vocation de présenter sous forme d'exposition virtuelle l'évolution des normes de sécurité entourant l'enfance dans une perspective historique, depuis la fin du XIXème siècle jusqu'à nos jours. S'appuyant sur les ressources mises en ligne par le Musée de Bretagne et la Cinémathèque de Bretagne, partenaires du projet, l'exposition présente au public plusieurs bandes dessinées. Chacune d'elle met en scène deux personnages, Prudence et son petit-fils Malo, qui explorent ensemble divers aspects du sujet: quelles étaient les recommandations médicales en matière d'alimentation des enfants ou de couchage des bébés, et ont-elles changé au cours du XXème siècle? De quelle façon les enfants s'amusaient-ils, et leurs jeux et jouets présentaient-ils un danger? Comment prenait-on soin de leur santé, et comment étaient-ils soignés s'ils tombaient malades? A travers les vieilles photographies et films familiaux que Prudence montre à son petit-fils, les enfants qui découvriront l'exposition avec leurs parents, grands-parents ou professeurs pourront partager la curiosité de Malo, et les adultes pourront leur transmettre les réponses grâce aux explications de Prudence. Des mini-jeux (puzzles, memorys et rébus) leur seront également proposés, et leur permettront de découvrir de façon ludique d'autres objets, images et films que comprennent les collections du Musée et de la Cinémathèque.<br />
Un projet en partenariat avec [Les Champs Libres](https://leschampslibres.fr), [le Musée de Bretagne](https://www.musee-bretagne.fr), [la Cinémathèque de Bretagne](https://www.cinematheque-bretagne.bzh) et [l'Université Rennes 2](https://sites-formations.univ-rennes2.fr/master-humanitesnumeriques/presentation/)

### Live Démo
:computer: https://prudence-exposition.go.yj.fr

### Éditorial
- À suivre sur https://www.facebook.com/prudenceHN
- Articles du carnet Hypothèses :
  - [Les enjeux scientifiques du projet](https://masterhnr2.hypotheses.org/59)
  - [Nos outils numériques](https://masterhnr2.hypotheses.org/68)
  - [Communication et valorisation] (https://masterhnr2.hypotheses.org/145)

### Contributeur.trice.s
Inès Bonnabot, Emmanuelle Connan-Perrot, Stevan Moisan, Apolline Morinière

### Technologies & logiciels
- Pour l'organisation des recherches historiques : [Notion](https://www.notion.so)
- Pour la création des planches de bande dessinée: [BDnF](https://bdnf.bnf.fr/)
- Pour la structuration et la présentation des bandes dessinées: [DocExplore](https://www.docexplore.eu/)
- Pour la création du site web: [WordPress](https://wordpress.com/fr/)
- Pour l'hébergement du site web: [PlanetHoster](https://www.planethoster.com/fr/)

[![Licence GNU/GPLv3](https://gitlab.com/leschampslibres-projetsweb-partenaires/universite-rennes2-master-humanites-numeriques/prudence/-/raw/master/gplv3-127x51.png)](https://www.gnu.org/licenses/quick-guide-gplv3.html)

---

> **CONTACT**<br />
> [Université Rennes 2<br />
> UFR Sciences sociales<br />
> Master Humanités Numériques](https://sites-formations.univ-rennes2.fr/master-humanitesnumeriques/contacts/)<br />
> :mortar_board: Promotion Hedy Lamarr | Master 2 2020-2021
